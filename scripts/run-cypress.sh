#!/bin/bash

/start-all.sh

wwwPath=${WWW_PATH//\//\\\/}
sed -i "s/\/var\/www\/html/$wwwPath/g" /etc/nginx/sites-enabled/default
service nginx start

rm $MOCHAWESOME_REPORTS_INPUT_DIR

pushd "$PROJECT_PATH"
    npx cypress run "$@"
    Result=$?
    if [ "$Result" != "0" ]; then echo "Test failed."; exit $Result; fi

    if [ "$MOCHAWESOME_MERGER" == "true" ]; then
        if [ "$MOCHAWESOME_INSTALL" == "true" ]; then
            npm install mochawesome --save-dev
        fi
        npx mochawesome-merge "$MOCHAWESOME_REPORTS_INPUT_DIR" > "$MOCHAWESOME_REPORTS_OUTPUT_DIR/$MOCHAWESOME_FULL_REPORTS_NAME.json"
        npx mochawesome-report-generator --reportDir "$MOCHAWESOME_REPORTS_OUTPUT_DIR" "$MOCHAWESOME_REPORTS_OUTPUT_DIR/$MOCHAWESOME_FULL_REPORTS_NAME.json"
    fi

    Result=$?
popd

exit $Result
