FROM cyancor/xserver-vnc:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENV NodeVersion="18" \
    TERM="xterm" \
    PROJECT_PATH="/project" \
    WWW_PATH="/www" \
    MOCHAWESOME_MERGER="" \
    MOCHAWESOME_REPORTS_INPUT_DIR="" \
    MOCHAWESOME_REPORTS_OUTPUT_DIR="" \
    MOCHAWESOME_INSTALL="" \
    MOCHAWESOME_FULL_REPORTS_NAME="full-report"

VOLUME [ "/project", "/www" ]

COPY scripts/run-cypress.sh /run-cypress.sh

RUN apt-get update \
    && apt-get install --yes \
        apt-utils\
        firefox \
        nginx \
        xvfb \
        sudo \
        # Fonts
        fonts-arphic-bkai00mp \
        fonts-arphic-bsmi00lp \
        fonts-arphic-gbsn00lp \
        fonts-arphic-gkai00mp \
        fonts-arphic-ukai \
        fonts-arphic-uming \
        ttf-wqy-zenhei \
        ttf-wqy-microhei \
        xfonts-wqy \
# Preparation
    && mkdir -p /info \
    && cat /etc/os-release | grep VERSION_ID | cut -d "\"" -f 2 > /info/ubuntu \
    && firefox --version | cut -d " " -f 3 > /info/firefox \
# Node.js
    && printf "\n\033[0;36mInstalling Node.js...\033[0m\n" \
    && curl -sL https://deb.nodesource.com/setup_$NodeVersion.x | bash - \
    && apt-get update \
    && apt-get install --yes nodejs \
    && npm update -g \
    && npm install -g npm@latest @angular/cli \
    && npm --version > /info/node \
# Chrome
    && printf "\n\033[0;36mInstalling Chrome...\033[0m\n" \
    && curl -LO https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && apt-get install -y ./google-chrome-stable_current_amd64.deb \
    && rm google-chrome-stable_current_amd64.deb \
    && google-chrome --version | cut -d " " -f 3 > /info/chrome \
# nginx
    && rm -rf /usr/share/nginx/html \
    && mkdir -p /www \
    && chmod 0777 /www \
    && ln -s /www /usr/share/nginx/html \
    && sed -i "s/user www-data;//g" /etc/nginx/nginx.conf \
    && sed -i "s/listen 80/listen 8080/g" /etc/nginx/sites-enabled/default \
    && sed -i "s/listen \[::\]:80/listen \[::\]:8080/g" /etc/nginx/sites-enabled/default \
    && nginx -v 2>&1 | cut -d " " -f 3 | cut -d "/" -f 2 > /info/nginx \
# Cypress.io
    && mkdir -p /tmp/project \
    && cd /tmp/project \
    && npm init -y \
    && npm install cypress \
    && npx cypress --version | head -1 | cut -d " " -f 4 > /info/cypress \
# Various configurations
    && mkdir -p /project \
    && chmod 0777 /project \
# Persmissions
    && chmod -R 0777 /usr/lib/node_modules \
# Cleanup
    && printf "\n\033[0;36mCleaning up...\033[0m\n" \
    && npm cache clean --force \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
# Done
    && printf "\n\033[0;36mDone.\033[0m\n"

CMD ["/run-cypress.sh"]

